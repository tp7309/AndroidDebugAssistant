from src.utils import tutil
import io
import os


def apply_clear_src():
    """清除源码中的非法字符"""
    symbols = {
        u'（': '(', '）': ')', '；': ';', '，': ','
    }
    path = 'C:\\Users\\jerrywangr\\Desktop\\JavaTest\\src\\com\\tp7309\\java\\Main.java'
    lines = []
    with io.open(path,  'r', encoding='utf-8') as f:
        lines = f.readlines()
    with io.open(path,  'w', encoding='utf-8') as f:
        newlines = []
        for line in lines:
            newline = line
            for k, v in symbols.items():
                newline = newline.replace(k, v)
            newlines.append(newline)
        f.writelines(newlines)


apply_clear_src()
