#!/usr/bin/env python3
# -*- coding: utf-8 -*-
# 聚体育辅助测试工具
# author: jerrywangr:17090632

import sys
import click
from src.utils import tutil
from src.utils.tutil import printcn
import inspect
import re
import os
import tconfig
import importlib
import time


_module_names = ['commands_test', 'commands_untest',
                 'commands_get', 'commands_clear', 'commands_start']
_time = time.time()


def collect_cmd_subcommands(m):
    functions = inspect.getmembers(m, inspect.isfunction)
    if not functions:
        return
    asserts = {}
    for name, func in functions:
        match = re.search(r'apply_\w+?_(\w+)', name)
        if match:
            asserts[match.group(1)] = func.__doc__ if func.__doc__ else ''
    return asserts


def update_config():
    prop = os.path.join(tutil.ROOT_PATH, 'local.properties')
    insert_option = ''
    content = ''
    if os.path.exists(prop):
        with open(prop, 'r') as f:
            content = f.read().strip()
            if 'mac=' not in content:
                insert_option = True
    with open(prop, 'w') as f:
        if insert_option:
            content = content + '\nip=\nmac='
        ip = re.search(r'ip\s?=[ ]?\S*', content).group()
        content = content.replace(ip, "ip=%s" % (tutil.extract_tvip()))
        macs = re.search(r'mac\s?=[ ]?\S*', content).group()
        mac = tutil.get_mac()
        if mac not in macs:
            sep = '' if macs.endswith('=') else ','
            if len(macs) > 30:
                content = content.replace(macs, 'mac=' + mac)
            else:
                content = content.replace(macs, sep.join([macs, mac]))
        f.write(content)


def getconfig(keys):
    prop = os.path.join(tutil.ROOT_PATH, 'local.properties')
    result = ['', '']
    if not os.path.exists(prop):
        return result
    with open(prop, 'r') as f:
        content = f.read()
        for idx, key in enumerate(keys):
            match = re.search(r"%s\s?=[ ]?(\S*)" % (key), content)
            if match:
                result[idx] = match.group(1)
    return result


def showhelp():
    help_info = [u"命令示例：p test tv", u"可用命令列表:"]
    _modules = {}
    for name in _module_names:
        module = importlib.import_module('src.' + name)
        _modules[name] = module

    for name, module in _modules.items():
        help_info.append(module.NAME + ':\n' +
                         tutil.formatDict(collect_cmd_subcommands(module)))
    printcn("\n\n".join(help_info) + '\n\n')


def apply(cmd, extra_args, can_retry=True):
    global _time
    # print("apply %dms" % ((time.time() - _time)*1000))
    _time = time.time()
    ip, macs = getconfig(['ip', 'mac'])

    if tconfig.CHECK_ADB_STATUS == 1:
        if not tutil.checkenv():
            # print("checkenv %dms" % ((time.time() - _time)*1000))
            _time = time.time()

            tutil.retry_connenv(ip, macs)
            # check again
            if not tutil.checkenv():
                printcn("无可用adb设备")
                update_config()
                return
            # reduce io operation
            update_config()
        elif not ip or not macs or (not tutil.extract_tvip() == ip):
            update_config()

    try:
        tutil.ensure_dir(tutil.CACHE_PATH)
        module = __import__('src.commands_' + cmd,
                            fromlist=('commands_' + cmd))
        func = getattr(module, "apply_%s_%s" % (cmd, extra_args[0]))
        # print("getattr %dms" % ((time.time() - _time)*1000))
        _time = time.time()
        printcn("正在%s..." % (func.__doc__))
        if(len(extra_args) > 1):
            func(extra_args[1:])
        else:
            func()

    except AttributeError as e:
        printcn(e)
        return
    printcn("命令处理完成")
    if tconfig.CHECK_ADB_STATUS == 1 and not tutil.checkenv() and can_retry:
        printcn("尝试连接adb中...")
        tutil.conn_env()
        if not tutil.checkenv():
            return
        apply(cmd, extra_args, can_retry=False)


@click.command()
@click.argument("command")
@click.argument("extra_args", nargs=-1)
def cli(command, extra_args):
    apply(command, extra_args)


if __name__ == "__main__":
    if sys.version[0] == '2':
        reload(sys)
        sys.setdefaultencoding("utf-8")
    if (len(sys.argv) > 1 and sys.argv[1] == "--help"):
        showhelp()
    else:
        cli()
