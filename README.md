# PPTVTestAssistant
Android应用辅助脚本。

#当前版本：
v1.0.0

# 使用要求
需要Python3.6及其以上。

# 初次使用使用
- 安装依赖包：
```
pip install -r requirements.txt
```
- 到config.py配置电视ip与聚体育包名。
- 将`当前目录\bin`加入系统PATH环境变量。


查看使用帮助：
```
p --help
```

# 示例
切换聚体育测网环境：
```
p test tv
```
