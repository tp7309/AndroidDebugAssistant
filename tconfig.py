#!/usr/bin/env python3
# -*- coding: utf-8 -*-

APP_ID = "com.pplive.androidxl.dev"  # 聚精彩包名
# APP_ID = "com.pplive.androidphone"  # PP视频包名
# APP_ID = "com.sshine.hello.tinker"

REPO_PATH = "C:\\Users\\tp730\\Desktop\\cibn-atv-2018"
# 检查adb状态会耗费大约2秒时间。
CHECK_ADB_STATUS = 0

CHANNELS = {
    'defaultChannel': ['sony:230047'],
    'preinstall': ['hisense_preinstall:230129', 'feixun:230132']
}
