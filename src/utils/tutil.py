#!usr/bin/env python3
# -*- encoding: utf-8 -*-
import subprocess
import os
import re
import time
import sys
import tconfig
sys.path.append(os.path.abspath(os.path.join(
    os.path.dirname(__file__), os.pardir, os.pardir, os.pardir)))


ROOT_PATH = os.path.abspath(os.path.join(os.path.dirname(os.path.abspath(__file__)),
                                         os.pardir, os.pardir))
DOC_PATH = os.path.join(ROOT_PATH, 'asserts')
CACHE_PATH = os.path.join(ROOT_PATH, 'cache')


if sys.version[0] == '2':
    reload(sys)
    sys.setdefaultencoding("utf-8")


def printcn(msg):
    if sys.version[0] == '2':
        sys_encoding = sys.getfilesystemencoding()
        print(msg.decode('utf-8').encode(sys_encoding))
    else:
        print(msg)


def open_file(f):
    open_command = 'xdg-open'  # linux?
    if os.name == 'nt':
        open_command = 'start'
    elif os.name == 'posix' or os.name == 'os2':
        open_command = 'open'
    sh("%s %s" % (open_command, f))


def run(command):
    printcn(command + '...')
    subprocess.call(command, shell=True)


def sh(command, print_msg=True):
    p = subprocess.Popen(
        command, shell=True, stdout=subprocess.PIPE, stderr=subprocess.STDOUT)
    result = p.stdout.read().decode('utf-8')
    if print_msg:
        printcn(result)
    return result


def get_app_gradle_path():
    return 'gradlew' if os.name == 'nt' else './gradlew'


def get_home_path():
    return os.path.expanduser('~')


def get_downloads_path():
    return os.path.join(get_home_path(), "downloads")


def adb_connect(ip, port):
    sh("adb connect %s:%s" % (ip, port))


def adb_disconnect(ip, port):
    sh("adb disconnect %s:%s" % (ip, port))


def check_adb_connectable_by_port(port, auto_disconnect=True):
    result = check_adb_connectable_by_ports(
        [port], auto_disconnect=auto_disconnect)
    return len(result) > 0 and result[0] == "127.0.0.1:%s" % (port)


def check_adb_connectable_by_ports(ports, auto_disconnect=True):
    devices = []
    ip = '127.0.0.1'
    for port in ports:
        device = "%s:%s" % (ip, port)
        os.system("adb connect %s" % (device))
        devices.append(device)
    result = check_adb_connected(devices)
    if auto_disconnect:
        for port in ports:
            os.system("adb disconnect %s" % (device))
    return result


def check_adb_connected(devices):
    # check if device is active
    active_result = sh("adb devices", print_msg=False)
    connected_devices = []
    for device in devices:
        if re.search(r"%s\s+device" % (device), active_result):
            connected_devices.append(device)
    return connected_devices


def is_port_opened(ip, port, timeout=1):
    result = sh("ssh -o ConnectTimeout=1 -p %d %s" % (port, ip))
    return "timed out" in result


def checkenv():
    # check if device is active
    active_result = sh("adb devices", print_msg=False)
    if "offline" in active_result:
        printcn(active_result)
        return False
    if len(active_result.strip().split('\n')) < 2:
        printcn(active_result)
        return False
    return True


def disconnet_env():
    sh("adb disconnect")


def conn_env():
    connect_result = sh("p test env")
    if "unable to connect" in connect_result:
        return False
    return True


def launch_app(appid=tconfig.APP_ID):
    # PPBOX P1上命令有兼容性问题，不显式指定Activity启动不了。
    # sh("adb shell monkey -p %s -c android.intent.category.LAUNCHER 1" %
    #    (appid))

    launcher = get_launcher_activity(package_name=appid)
    if not launcher:
        print('launcher name can not found')
        return
    sh("adb shell am start -n %s" % (launcher))


def restart_app(appid=tconfig.APP_ID):
    force_stop_app(appid)
    launch_app(appid)


def force_stop_app(appid=tconfig.APP_ID):
    sh("adb shell am force-stop %s" % (appid))


def formatDict(d):
    return '\n'.join(['%s = %s' % (key, value) for (key, value) in d.items()])


def asserts(filename):
    return os.path.join(DOC_PATH, filename)


def cache(filename):
    return os.path.join(CACHE_PATH, filename)


def get_mac():
    mac = sh("adb shell cat /sys/class/net/eth0/address", print_msg=False)
    match = re.match(r'(\w{2}:){5}\w{2}', mac.strip())
    if not match:
        mac = sh("adb shell cat /sys/class/net/wlan0/address", print_msg=False)
    match = re.match(r'(\w{2}:){5}\w{2}', mac.strip())
    return match.group() if match else ''


def extract_tvip(src=''):
    if not src:
        src = sh("adb devices", print_msg=False)
    # adb port may be not default
    match = re.search(r'(\d{1,3}\.){3}\d{1,3}(:\d{3,5})?', src)
    return match.group(0) if match else ''


def rooted():
    return 'Permission denied' not in sh("adb shell ls /data/data")


def isppos():
    return 'PPTV' in sh("adb shell getprop ro.product.manufacturer", print_msg=False)


def retry_connenv(ip, macs):
    print("try connect tv...")
    if ip:
        run("adb connect %s" % (ip))
    if not checkenv():
        addrs = sh("arp -a", print_msg=False).split('\n')
        if not macs:
            return
        macs = macs.split(',')
        for addr in addrs:
            # windows
            addr = addr.replace('-', ':')
            for mac in macs:
                if mac in addr:
                    ip = extract_tvip(addr)
                    run("adb connect %s" % (ip))
                    break


def usbpath():
    if os.name == 'nt':
        disks = sh("wmic logicaldisk get deviceid, description",
                   print_msg=False).split('\n')
        for disk in disks:
            if 'Removable' in disk:
                return re.search(r'\w:', disk).group()
    elif os.name == 'posix':
        return sh('ll -a /media')[-1].strip()
    else:
        return sh('ls /Volumes')[-1].strip()


def get_package_name(apkpath):
    if not os.path.exists(apkpath):
        return ''
    result = None
    if os.name == 'nt':
        result = sh('aapt dump badging %s | findstr "version"' % (apkpath))
    else:
        result = sh('aapt dump badging %s | grep "version"' % (apkpath))
    if result:
        match = re.search(r'package:\s+name=\'(.+?)\'', result)
        if match:
            return match.group(1)


def safe_index_of(str0, substr):
    try:
        return str0.index(substr)
    except ValueError:
        return -1


def get_launcher_activity(package_name=tconfig.APP_ID):
    result = sh("adb shell dumpsys package %s" %
                (package_name), print_msg=False)
    if not result:
        return
    end_index = safe_index_of(result, "android.intent.category.LAUNCHER")
    if end_index >= 0:
        start_index = (end_index - 150) if end_index - 150 >= 0 else 0
        lines = result[start_index:end_index].split(' ')
        for line in lines:
            if package_name in line:
                return line.strip()

    start_index = safe_index_of(result, "android.intent.action.MAIN")
    if start_index >= 0:
        end_index = (start_index + 300) if (start_index +
                                            300 < len(result)) else len(result)
        lines = result[start_index:end_index].split(' ')
        key = "%s/" % (package_name)
        for line in lines:
            # APP包名可能与启动Activity类所在的包不一致，先检查此种情况。
            if '/com.' in line:
                if "/%s" % (package_name) in line:
                    return line.strip()
            if key in line:
                return line.strip()
    return ''


def ensure_dir(dirpath):
    if not os.path.exists(dirpath):
        os.makedirs(dirpath)


def py27_path():
    cmd = "python"
    if sys.version_info >= (3, 0):
        cmd = "conda activate py27 && python"
    return cmd


def currtime():
    return time.strftime("%Y%m%d-%H%M%S")


def getpid():
    result = sh('adb shell ps | grep "%s" | head -1' % (tconfig.APP_ID))
    pids = re.compile(r'\s+').split(result)
    return pids[1] if pids else ""


def query_yes_no(question, default="yes"):
    """Ask a yes/no question via raw_input() and return their answer.

    "question" is a string that is presented to the user.
    "default" is the presumed answer if the user just hits <Enter>.
        It must be "yes" (the default), "no" or None (meaning
        an answer is required of the user).

    The "answer" return value is True for "yes" or False for "no".
    """
    valid = {"yes": True, "y": True, "ye": True,
             "no": False, "n": False}
    if default is None:
        prompt = " [y/n] "
    elif default == "yes":
        prompt = " [Y/n] "
    elif default == "no":
        prompt = " [y/N] "
    else:
        raise ValueError("invalid default answer: '%s'" % default)

    while True:
        sys.stdout.write(question + prompt)
        if sys.version_info < (3, 0):
            choice = raw_input().lower()
        else:
            choice = input().lower()
        if default is not None and choice == '':
            return valid[default]
        elif choice in valid:
            return valid[choice]
        else:
            sys.stdout.write("Please respond with 'yes' or 'no' "
                             "(or 'y' or 'n').\n")
