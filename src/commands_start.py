#!/usr/bin/env python3
# -*- coding: utf-8 -*-

from __future__ import print_function
import glob
import os
import subprocess
import sys
import time
import re
import io
import json
from zipfile import ZipFile

try:
    from pylab import mpl
    import matplotlib.pyplot as plt
    import numpy as np
except ImportError:
    print('can not load matplotlib')


import tconfig
from src.utils import tutil
from src.utils.tutil import run, sh, printcn

sys.path.append(os.path.abspath(os.path.join(
    os.path.dirname(__file__), os.pardir, os.pardir, os.pardir)))


NAME = 'start'


def apply_start_settings():
    """打开系统原生设备界面"""
    run('adb shell am start -n com.android.settings/com.android.settings.Settings')


def apply_start_tcpdump():
    """安装并启动tcpdump"""
    run('adb root')
    if 'tcpdump' not in sh('adb shell ls /data/local', print_msg=False):
        run("adb push %s /data/local/tcpdump " % (tutil.asserts('tcpdump')))
        run("adb shell chmod 6755 /data/local/tcpdump")
    run('adb shell /data/local/tcpdump -i any -p -s 0 -w /sdcard/net.pcap')


def apply_start_tv():
    """启动聚体育"""
    tutil.launch_app()


def apply_start_appinfo():
    """启动应用信息界面"""
    run("adb shell am start -a android.settings.APPLICATION_DETAILS_SETTINGS package:%s" %
        (tconfig.APP_ID))


def apply_start_adbkey():
    """打开adb keyboard"""
    run("java -jar %s && exit" %
        (tutil.asserts('AdbKeyMonkey-1.0.4.jar')))


def apply_start_ota():
    """进行OTA升级"""
    model = sh("adb shell getprop ro.product.model", print_msg=False)
    print("设备型号：%s" % (model))
    # Q1盒子
    if 'PPBOX Q1' in model:
        run("adb install -r %s" % (tutil.asserts('q1-OTAUpgrade.apk')))
        packagename = tutil.get_package_name(
            tutil.asserts('q1-OTAUpgrade.apk'))
        tutil.launch_app(appid=packagename)


def apply_start_run():
    """安装并打开聚精彩安装包"""
    original_dir = os.getcwd()
    os.chdir(tconfig.REPO_PATH)
    run("gradlew :launcher_v4:launcher_v4:assembleDevelopDebug")
    apkdir = os.path.join('launcher_v4', 'launcher_v4',
                          'build', 'outputs', 'apk', 'develop', 'debug')
    apks = glob.glob("%s/*.apk" % (apkdir))
    if not apks:
        return
    run("adb install -r -t %s" % (apks[0]))
    tutil.launch_app()
    os.chdir(original_dir)


def apply_start_rename():
    """批量修改文件名"""
    face_dir = "C:\\Work\\Face"
    for root, dirs, files in os.walk(face_dir):
        for file in files:
            if file.endswith('jfif'):
                newname = os.path.splitext(file)[0] + '111.png'
                os.rename(os.path.join(root, file),
                          os.path.join(root, newname))


def apply_start_autotest():
    """启动自动测试脚本"""
    bakDir = os.getcwd()
    os.chdir("D:\\Work\\Store\\自动化\\autotest_TVsports")
    print("curdir: " + os.getcwd())
    ip = tutil.extract_tvip().split(':')[0]
    if (tconfig.APP_ID.endswith('.dev')):
        subprocess.call("python autotest_jjc.py " + ip)
    else:
        subprocess.call("python autotest_jjc0423-nodev.py " + ip)
    os.chdir(bakDir)


def apply_start_fdmonitor():
    """启动APP句柄打开数监控"""
    result = sh('adb shell ps | grep "%s" | head -1' % (tconfig.APP_ID))
    pids = re.compile(r'\s+').split(result)
    if not pids:
        print("can not find app")
        return
    pid = pids[1]
    arrx = []
    arry = []
    try:
        count = 0
        while(True):
            # lsof -p pid这命令好像有问题，没有按pid过滤。
            fd_count = sh("adb shell ls /proc/%s/fd %s | wc -l" %
                          (pid, pid), print_msg=False).strip()
            count += 1
            # 单行输出
            print(fd_count, end=' ', flush=True)
            arrx.append(count)
            arry.append(int(fd_count))
            time.sleep(0.2)
    except KeyboardInterrupt:
        # build chart
        mpl.rcParams['font.sans-serif'] = ['FangSong']  # 指定默认字体
        mpl.rcParams['axes.unicode_minus'] = False  # 解决保存图像是负号'-'显示为方块的问题
        fig, ax = plt.subplots()
        # 设置数据
        ax.plot(arrx, arry)
        # plt.yticks(np.arange(0, max(arry), 10))  //设置刻度
        # plt.ylim(300, 1024)   //设置y坐标轴范围，y数组必须是数字，这样才能有效！
        ax.set(xlabel='时间(s)', ylabel='句柄数量', title="句柄打开数量监控")
        fig.savefig(tutil.cache('fd_monitor.png'))
        result = plt.show()


def getrespaths(search_path, resdirs=['drawable', 'minmap']):
    respaths = []
    exclude_prefixs = ['.gradle', '.idea', 'asserts',
                       'build', 'androidTest', 'test', 'java']
    for root, dirs, filenames in os.walk(search_path, topdown=True):
        dirs[:] = [d for d in dirs if d not in exclude_prefixs]
        for dirname in dirs:
            for resdir in resdirs:
                if dirname.startswith(resdir):
                    respaths.append(os.path.join(root, dirname))
    return respaths


def apply_start_reszip():
    """压缩应用内的图片资源文件"""
    path = "C:\\Users\\tp730\\Desktop\\cibn-atv-2018\\atv_sports\\src\\main\\res"
    respaths = getrespaths(path)
    print('found res paths:\n')
    print('\n'.join(respaths))

    count = 0
    index = 0
    zippath = os.path.abspath(os.path.join(
        path, os.path.pardir, "%d.zip" % (index)))
    print(zippath)
    print('---------------------------------')
    mapping = []
    f = ZipFile(zippath, 'w')
    for respath in respaths:
        for root, dirs, files in os.walk(respath):
            for filename in files:
                if filename.endswith('.9.png'):
                    continue
                count += 1
                filepath = os.path.join(root, filename)
                if count // 20 > index:
                    f.close()
                    index += 1
                    zippath = os.path.abspath(os.path.join(
                        path, os.path.pardir, "%d.zip" % (index)))
                    print(zippath)
                    f = ZipFile(zippath, 'w')
                f.write(filepath, filename)
                mapping.append([filename, filepath])
    f.close()
    # save mapping
    with io.open('res_mapping.json', 'w', encoding='utf-8') as f:
        f.write(json.dumps(mapping))


def apply_start_matrix_clean():
    """按照matrix规则处理无用资源"""
    matrix_json_path = "D:\\Work\\apk-checker-result.json"
    # 获取图片相关资源
    respaths = getrespaths(tconfig.REPO_PATH)
    print('found res paths:\n')
    print('\n'.join(respaths))
    # 获取layout相关资源
    layout_paths = getrespaths(tconfig.REPO_PATH, ['layout'])
    print('--------------------------------------------')
    unused_resources = []
    with io.open(matrix_json_path, encoding='utf-8') as f:
        data = json.load(f)
        for task_entry in data:
            if task_entry['taskType'] == 12:
                unused_resources = task_entry['unused-resources']
                break

    def getrespath(resid):
        exts = []
        searchpaths = []
        if resid.startswith('R.layout'):
            exts = ['.xml']
            searchpaths = layout_paths
        else:
            exts = ['.9.png', '.png', '.jpg', '.webp']
            searchpaths = respaths
        resname = resid.replace('R.drawable.', '').replace('R.layout.', '')
        for path0 in searchpaths:
            for ext in exts:
                path = os.path.join(path0, resname) + ext
                if os.path.exists(path):
                    return path
        return ""

    with io.open('deleted_resources.txt', 'w', encoding='utf-8') as f:
        for resid in unused_resources:
            if not (resid.startswith('R.drawable') or resid.startswith('R.layout')):
                continue
            respath = getrespath(resid)
            if not respath:
                continue
            print("delete " + respath)
            f.write(respath)
            f.write('\n')
            try:
                os.remove(respath)
            except Exception:
                print("unable to delete " + respath)
    print("\n\ndeleted files are recorded in 'deleted_resources.txt'\n")


def apply_start_systrace():
    """开启systrace，TAG为WRF, env: py27"""
    printcn("systrace启动后再开启操作界面，否则数据会不准确。")
    systrace_path = os.path.join(
        os.environ['ANDROID_HOME'], 'platform-tools', 'systrace', 'systrace.py')
    output_path = os.path.join(
        tutil.get_downloads_path(), "systrace_%s.html" % (tutil.currtime()))
    cmd = "python %s -t 5 --no-compress sched gfx view wm am res webview video -o %s -a %s" % (
    # cmd = "python %s -t 5 --no-compress gfx view wm am video -o %s -a %s" % (
        systrace_path, output_path, tconfig.APP_ID)
    tutil.run(cmd)
    if os.path.exists(output_path):
        tutil.open_file(output_path)


def apply_start_update_ftime(args):
    """更新文件夹中所有文件的创建时间, FLIE_PATH"""
    path = args[0]
    run("powershell.exe -command \"Get-Childitem -path \'" + path
        + "\' -Recurse | foreach-object { $_.LastWriteTime = Get-Date;"
        + " $_.CreationTime = Get-Date }\"")
