#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import os
from src.utils import tutil
from src.utils.tutil import sh, run, printcn
import io
import glob
import tconfig
import random
import datetime
import time


NAME = 'test'


def apply_test_tv():
    """切换到聚体育测试环境"""
    pptv_dir = "/sdcard/pptv_sports"
    sh("adb shell rm -r %s" % (pptv_dir), print_msg=False)
    sh("adb shell mkdir %s" % (pptv_dir))
    sh("adb shell touch %s/internal" % (pptv_dir))
    tutil.restart_app()


def apply_test_atv():
    """切换到聚精彩测试环境"""
    atv_dir = "/sdcard/pptv_atv"
    sh("adb shell rm -r %s" % (atv_dir), print_msg=False)
    sh("adb shell mkdir %s" % (atv_dir))
    sh("adb shell touch %s/sit" % (atv_dir))
    apply_test_tv()


def apply_test_atvpre():
    """切换到聚精彩pre环境"""
    atv_dir = "/sdcard/pptv_atv"
    sh("adb shell rm -r %s" % (atv_dir), print_msg=False)
    sh("adb shell mkdir %s" % (atv_dir))
    sh("adb shell touch %s/sit" % (atv_dir))
    tutil.restart_app()


def apply_test_atvproxy():
    """在本地开启聚精彩域名跳转服务"""
    atv_dir = "/sdcard/pptv_atv"
    sh("adb shell mkdir %s" % (atv_dir), print_msg=False)
    sh("adb shell touch %s/atvproxy" % (atv_dir))
    tutil.restart_app()


def apply_test_ppos():
    """设置PPOS测试环境"""
    ppos_settings = os.path.join(tutil.DOC_PATH, "launcher_setting.xml")
    run("adb push %s /data/data/com.pptv.launcher/shared_prefs/" %
        (ppos_settings))
    tutil.force_stop_app(appid="com.pptv.launcher")


def apply_test_uri():
    """安装测试Uri跳转apk"""
    apk_path = os.path.join(tutil.DOC_PATH, "TVSports-start-demo.apk")
    run("adb install -r %s" % (apk_path))
    tutil.launch_app(appid="com.ccbfm.testurl_tvsports")


def apply_test_actionuri():
    """测试Uri跳转"""
    uri = "pptv_tvsports://tvsports_competition_data?competition_id=7&from_internal=1"
    # 不在adb shell下运行时有兼容性问题
    uri.replace('&', "'&'")
    run('adb shell am start -W -a "android.intent.action.VIEW" -d "%d"' % (uri))


def apply_test_push():
    """设置PushSDK测试环境"""
    push_settings = os.path.join(tutil.DOC_PATH, 'pushservice_setting.xml')
    sh("adb push %s /data/data/com.pptv.push.service/shared_prefs" % (push_settings))
    tutil.force_stop_app(appid="com.pptv.push.service")


def apply_test_tvad():
    """加入聚体育广告hosts"""
    adhosts = os.path.join(tutil.DOC_PATH, 'tvsports_ad_hosts')
    result = sh("adb push %s /system/etc/hosts" % (adhosts))
    if 'Read-only file system' in result:
        run('p clear readonly')
        apply_test_tvad()


def apply_test_atvad():
    """加入聚精彩广告hosts"""
    adhosts = os.path.join(tutil.DOC_PATH, 'atv_ad_hosts')
    result = sh("adb push %s /system/etc/hosts" % (adhosts))
    if 'Read-only file system' in result:
        run('p clear readonly')
        apply_test_tvad()


def apply_test_loginpre():
    """聚精彩登录切换到pre环境"""
    hosts = os.path.join(tutil.DOC_PATH, 'login_pre_hosts')
    result = sh("adb push %s /system/etc/hosts" % (hosts))
    if 'Read-only file system' in result:
        run('p clear readonly')
        apply_test_loginpre()
        return
    atv_dir = "/sdcard/pptv_atv"
    sh("adb shell mkdir %s" % (atv_dir), print_msg=False)
    sh("adb shell touch %s/svip" % (atv_dir))
    tutil.restart_app()


def apply_test_env():
    """测试用adb连接设备"""
    if not tutil.checkenv():
        tutil.disconnet_env()
        tutil.conn_env()


def replace_content(file, content_map):
    content = ''
    with io.open(file, 'r', encoding='utf-8') as f:
        content = f.read()
    for k, v in content_map.items():
        content = content.replace(k, v)
    with io.open(file, 'w', encoding='utf-8') as f:
        f.write(content)


def apply_test_debugon():
    """为聚体育加入秒速构建支持"""
    appid = 'com.pptv.tvsports.dev'
    run("adb uninstall %s" % (appid))
    holder_apk = 'com.pptv.tvsports.dev_debug_1.0.apk'
    holder_apk_path = tutil.asserts(holder_apk)
    run('p clear readonly')
    dest_dir = '/system/app'
    result = sh("adb shell ls %s/%s" %
                (dest_dir, holder_apk), print_msg=False)
    if 'No such file' in result:
        dest_dir1 = "/system/app/%s" % (appid)
        result1 = sh('adb shell "ls %s"' %
                     (dest_dir1), print_msg=False)
        if 'No such file' in result1:
            run("adb push %s %s" % (holder_apk_path, dest_dir))
        else:
            run("adb push %s %s" % (holder_apk_path, dest_dir1))
            printcn('已找到的占位包...')
            printcn(result)
    else:
        printcn('已找到的占位包...')
        printcn(result)

    print("current dir:%s" % (os.getcwd()))
    appfile = os.path.join(os.getcwd(), 'app', 'src',
                           'main', 'java', 'com', 'pptv',
                           'tvsports', 'common',
                           'CommonApplication.java')
    if not os.path.exists(appfile):
        print("can not find %s!" % (appfile))
        return
    space4 = '    '
    sep = '\n' + space4
    sep8 = '\n' + space4 + space4
    appfile_enable_map = {
        "\nimport com.pptv.tvsports.common.utils.FreelineCompatApplicationLike;":
        "\nimport com.pptv.tvsports.common.utils.FreelineCompatApplication;",
        "\npublic class CommonApplication extends FreelineCompatApplicationLike {":
        "\npublic class CommonApplication extends FreelineCompatApplication {",
        ("/*FREELINE_COMPAT%spublic CommonApplication() {%s%ssuper();"
         + "%s}%sFREELINE_COMPAT_END*/\n\n%spublic CommonApplication(") %
        (sep, sep, space4, sep, sep, space4):
        "public CommonApplication() {%s%ssuper();%s}\n\n%spublic CommonApplication(" % (
            sep, space4, sep, space4),

        "\nimport com.facebook.stetho.Stetho;": "",
        "GlideUrl;":
        "GlideUrl;\nimport com.facebook.stetho.Stetho;",
        "mContext = getApplication();%sFreelineCore.init(getApplication());" % (sep8):
        ("mContext = getApplication();%sStetho.initializeWithDefaults(getApplication());"
         + "%sFreelineCore.init(getApplication());") % (sep8, sep8),
    }
    replace_content(appfile, appfile_enable_map)

    http_factory_file = os.path.join(os.getcwd(), 'app', 'src',
                                     'main', 'java', 'com', 'pptv',
                                     'tvsports', 'sender',
                                     'OkHttpFactory.java')
    http_factory_enable_map = {
        "import com.facebook.stetho.okhttp3.StethoInterceptor;": "",
        "import com.pptv.tvsports.common.CommonApplication;":
        "import com.facebook.stetho.okhttp3.StethoInterceptor;"
        + "import com.pptv.tvsports.common.CommonApplication;",
        ".addNetworkInterceptor(new StethoInterceptor())": "",
        ".cache(cache);":
        ".addNetworkInterceptor(new StethoInterceptor()).cache(cache);"
    }
    replace_content(http_factory_file, http_factory_enable_map)

    gradle = os.path.join(os.getcwd(), 'app', 'build.gradle')
    gradle_enable_map = {
        "\napply from: 'tinker.gradle'":
        "\n//apply from: 'tinker.gradle'",
        ("debugImplementation 'com.facebook.stetho:stetho:1.5.0'%s"
         + "debugImplementation 'com.facebook.stetho:stetho-okhttp3:1.5.0'\n") % (sep): "",
        "implementation 'com.tencent.bugly:crashreport:":
        ("debugImplementation 'com.facebook.stetho:stetho:1.5.0'%s"
             + "debugImplementation 'com.facebook.stetho:stetho-okhttp3:1.5.0'%s"
             + "implementation 'com.tencent.bugly:crashreport:") % (sep, sep)
    }
    replace_content(gradle, gradle_enable_map)


def apply_untest_debug():
    """去除聚体育秒速构建支付文件修改"""
    print("current dir:%s" % (os.getcwd()))
    appfile = os.path.join(os.getcwd(), 'app', 'src',
                           'main', 'java', 'com', 'pptv',
                           'tvsports', 'common',
                           'CommonApplication.java')
    if not os.path.exists(appfile):
        print("can not find %s!" % (appfile))
        return
    space4 = '    '
    sep = '\n' + space4
    sep8 = '\n' + space4 + space4
    appfile_enable_map = {
        "\nimport com.pptv.tvsports.common.utils.FreelineCompatApplicationLike;":
        "\nimport com.pptv.tvsports.common.utils.FreelineCompatApplication;",
        "\npublic class CommonApplication extends FreelineCompatApplicationLike {":
        "\npublic class CommonApplication extends FreelineCompatApplication {",
        ("/*FREELINE_COMPAT%spublic CommonApplication() {%s%ssuper();"
         + "%s}%sFREELINE_COMPAT_END*/\n\n%spublic CommonApplication(") %
        (sep, sep, space4, sep, sep, space4):
        "public CommonApplication() {%s%ssuper();%s}\n\n%spublic CommonApplication(" % (
            sep, space4, sep, space4),

        "\nimport com.facebook.stetho.Stetho;": "",
        "GlideUrl;":
        "GlideUrl;\nimport com.facebook.stetho.Stetho;",
        "mContext = getApplication();%sFreelineCore.init(getApplication());" % (sep8):
        ("mContext = getApplication();%sStetho.initializeWithDefaults(getApplication());"
         + "%sFreelineCore.init(getApplication());") % (sep8, sep8),
    }
    replace_content(appfile, appfile_enable_map)

    http_factory_file = os.path.join(os.getcwd(), 'app', 'src',
                                     'main', 'java', 'com', 'pptv',
                                     'tvsports', 'sender',
                                     'OkHttpFactory.java')
    http_factory_enable_map = {
        "import com.facebook.stetho.okhttp3.StethoInterceptor;": "",
        "import com.pptv.tvsports.common.CommonApplication;":
        "import com.facebook.stetho.okhttp3.StethoInterceptor;"
        + "import com.pptv.tvsports.common.CommonApplication;",
        ".addNetworkInterceptor(new StethoInterceptor())": "",
        ".cache(cache);":
        ".addNetworkInterceptor(new StethoInterceptor()).cache(cache);"
    }
    replace_content(http_factory_file, http_factory_enable_map)

    gradle = os.path.join(os.getcwd(), 'app', 'build.gradle')
    gradle_enable_map = {
        "\napply from: 'tinker.gradle'":
        "\n//apply from: 'tinker.gradle'",
        ("debugImplementation 'com.facebook.stetho:stetho:1.5.0'%s"
         + "debugImplementation 'com.facebook.stetho:stetho-okhttp3:1.5.0'") % (sep): "",
        "implementation 'com.tencent.bugly:crashreport:":
        ("debugImplementation 'com.facebook.stetho:stetho:1.5.0'%s"
             + "debugImplementation 'com.facebook.stetho:stetho-okhttp3:1.5.0'%s"
             + "implementation 'com.tencent.bugly:crashreport:") % (sep, sep)
    }
    replace_content(gradle, gradle_enable_map)


def apply_test_debugf():
    """Freeline强制全量编译"""
    apply_test_debugon()
    gradle = tutil.get_app_gradle_path()
    if not os.path.exists('freeline_project_description.json'):
        run("%s initFreeline" % (gradle))
    run('python freeline.py -f')


def apply_test_debug():
    """进行Freeline增量编译"""
    run('python freeline.py')


def apply_test_patch():
    """本地测试patch"""
    work_dir = os.path.join(tutil.get_downloads_path(), 'test')
    run('p clear app')
    apks = glob.glob("%s/*.apk" % (work_dir))
    if len(apks) >= 2:
        if '_patch_' in apks[0]:
            main_apk, patch = apks[1], apks[0]
        else:
            main_apk, patch = apks[0], apks[1]
    run("adb install -r %s" % (main_apk))
    run("adb push %s /sdcard/patch_signed_7zip.apk" % (patch))
    apply_test_atv()


def apply_test_tvlog():
    """打开聚体育OKHttp的Log打印"""
    run('adb shell setprop log.tvsports.DEBUG_OKHTTP 1')
    tutil.restart_app()


def apply_test_snsdk():
    """"测试苏宁sdk启动日志上报"""
    repeat_time = 10
    wait_times = [0, 3, 5, 10, 45, 80]
    uris = [
        'pptv_tvsports://tvsports_diy?from_internal=1'
    ]
    run("adb shell am force-stop %s" % (tconfig.APP_ID))
    print("start time: " + str(datetime.datetime.now()))

    uri_index = 0

    def nexturi(uri_index):
        return uris[uri_index]
        # uri_index = 1 if uri_index == 0 else 0
        # return uris[uri_index]

    for i in range(0, repeat_time):
        print("%sindex: %d" % (os.linesep, i + 1))
        run('adb shell am start -a "android.intent.action.VIEW" -d "%s"' %
            (nexturi(uri_index)))
        wait_time = wait_times[random.randint(0, len(wait_times) - 1)]
        print("wait %ds..." % (wait_time))
        if wait_time == 0:
            run("adb shell am force-stop %s" % (tconfig.APP_ID))
        else:
            time.sleep(wait_time)
        run('adb shell input keyevent 4')  # KEYCODE_BACK
        time.sleep(5)  # 每次启动间隔5秒
