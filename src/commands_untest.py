#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import os
from src.utils import tutil
from src.utils.tutil import sh, run
import io
import tconfig

NAME = 'untest'


def apply_untest_tv(restart_app=True):
    """取消聚体育测试环境"""
    pptv_dir = "/sdcard/pptv_sports"
    sh("adb shell rm -r %s" % (pptv_dir), print_msg=False)
    if restart_app:
        tutil.restart_app()


def apply_untest_atv():
    """取消聚精彩测试环境"""
    atv_dir = "/sdcard/pptv_atv"
    sh("adb shell rm -r %s/sit" % (atv_dir), print_msg=False)
    apply_untest_tv()


def apply_test_atvpre():
    """取消聚精彩pre环境"""
    atv_dir = "/sdcard/pptv_atv"
    sh("adb shell rm -r %s/sit" % (atv_dir), print_msg=False)
    tutil.restart_app()


def apply_untest_atvproxy():
    """关闭聚精彩域名跳转服务"""
    atv_dir = "/sdcard/pptv_atv/"
    sh("adb shell rm -r %s/atvproxy" % (atv_dir), print_msg=False)
    tutil.restart_app()


def apply_untest_uri():
    """卸载安装测试Uri跳转apk"""
    sh("adb uninstall com.ccbfm.testurl_tvsports")


def apply_untest_ppos():
    """取消PPOS测试环境"""
    sh("adb shell rm -r /data/data/com.pptv.launcher/shared_prefs/launcher_setting.xml"
       )
    tutil.force_stop_app(appid="com.pptv.launcher")


def apply_untest_push():
    """取消PushSDK测试环境"""
    sh("adb shell rm -r data/data/com.pptv.push.service/shared_prefs/pushservice_setting.xml")
    tutil.force_stop_app(appid="com.pptv.push.service")


def apply_untest_tvad():
    """取消聚体育广告hosts"""
    noadhosts = os.path.join(tutil.DOC_PATH, 'tvsports_no_ad_hosts')
    result = sh("adb push %s /system/etc/hosts" % (noadhosts))
    if 'Read-only file system' in result:
        run('p clear readonly')
        apply_untest_tvad()


def apply_untest_loginpre():
    """聚精彩登录取消pre环境"""
    noadhosts = os.path.join(tutil.DOC_PATH, 'tvsports_no_ad_hosts')
    result = sh("adb push %s /system/etc/hosts" % (noadhosts))
    if 'Read-only file system' in result:
        run('p clear readonly')
        apply_untest_loginpre()
        return
    atv_dir = "/sdcard/pptv_atv/"
    sh("adb shell rm -r %s/svip" % (atv_dir), print_msg=False)
    tutil.restart_app()


def replace_content(file, content_map):
    content = ''
    with io.open(file, 'r', encoding='utf-8') as f:
        content = f.read()
    for k, v in content_map.items():
        if v:
            content = content.replace(v, k)
    with io.open(file, 'w', encoding='utf-8') as f:
        f.write(content)


def apply_untest_tvlog():
    """关闭OKHttp的Log打印"""
    run('adb shell setprop log.tvsports.DEBUG_OKHTTP 0')
    tutil.restart_app()
