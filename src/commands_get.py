#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import sys
import os
import re
import shutil
from src.utils import tutil
from src.utils.tutil import sh, run, printcn
from . tv_prop import get_os_prop
from . tv_prop import OUTPUT as PROP_OUTPUT
import xml.etree.ElementTree as ET
import glob
import tconfig
import io
import time
sys.path.append(os.path.abspath(os.path.join(
    os.path.dirname(__file__), os.pardir, os.pardir, os.pardir)))

NAME = 'get'


def apply_get_anrlog():
    """获取anr日志"""
    sh("adb pull get /data/data/%s/cache/tvsports_crash.log" % (tconfig.APP_ID),
       print_msg=True)
    sh("adb pull /data/anr/traces.txt")
    cache_dir = os.path.join(
        os.path.dirname(os.path.abspath(__file__)), "cache")
    if not os.path.exists(cache_dir):
        os.makedirs(cache_dir)
    if os.path.exists('tvsports_crash.log'):
        shutil.move("tvsports_crash.log",
                    os.path.join(cache_dir, "tvsports_crash.log"))
    shutil.move("traces.txt", os.path.join(cache_dir, "traces.txt"))
    sh("start " + os.path.join(cache_dir, "traces.txt"))


def apply_get_start_time():
    """获取应用平均启动时间"""
    repeat_time = 10
    printcn("共统计%d次" % (repeat_time))
    timeFile = "start_time.txt"
    launcher = tutil.get_launcher_activity()
    # remove first launch time
    run("adb shell am start -S -R %d -W  %s > %s" %
        (repeat_time, launcher, timeFile))
    # time.sleep(3)
    re_time = r"TotalTime:\s+(\d+)"
    with io.open(timeFile, 'r', encoding='utf-8') as f:
        matches = re.findall(re_time, f.read())
        if matches and len(matches) > 0:
            matches = matches[1:]
        printcn("启动时间统计：")
        printcn(matches)

        sum_time = 0
        for time1 in matches:
            sum_time += int(time1)
        printcn("平均启动时间：%d" % (sum_time / max(len(matches), 1)))

    os.remove(timeFile)


def apply_get_screenshot():
    """获取屏幕截图"""
    sh("adb shell screencap -p /sdcard/screenshot.png")
    sh("adb pull /sdcard/screenshot.png .", print_msg=False)
    dest_path = "asserts/screenshot.png"
    shutil.move("screenshot.png", dest_path)
    tutil.open_file(dest_path)


def apply_get_prop():
    """获取电视系统属性"""
    get_os_prop()
    tutil.open_file(PROP_OUTPUT)


def apply_get_applog():
    """获取app.log日志"""
    run("adb pull /data/data/%s/cache/applogcache.log" % (tconfig.APP_ID))
    tutil.open_file('applogcache.log')


def apply_get_patchlog():
    """获取patch包更新说明"""
    patch_log = tutil.asserts('patch_log.txt')
    with io.open(patch_log, 'w', encoding='utf-8') as f:
        f.write("更新说明:\n")
        # old_tinker_id = patch.split('_')[1]
        # new_tinker_id = patch.split('_')[-1].rstrip('.apk')
        old_tinker_id = '67f648a'
        new_tinker_id = 'HEAD'
        logs = sh("git log %s..%s --oneline" %
                  (old_tinker_id, new_tinker_id),
                  print_msg=False).split('\n')
        result_logs = []
        for log in logs:
            log = ' '.join(log.split(' ')[1:])
            if 'Merge branch' not in log and \
                    'Merge remote-tracking branch' not in log:
                result_logs.append(log)
        f.write('\n'.join(result_logs))
        f.write(os.linesep + os.linesep)
    tutil.open_file(patch_log)


# python3
def apply_get_miguaars():
    """获取咪咕集成所依赖的所有jar包"""
    store_dir = os.path.join(os.path.expanduser(
        '~'), 'Downloads', 'LibDependencies')
    shutil.rmtree(store_dir, ignore_errors=True)

    copy_dependencies(
        'com.pptv.ottplayer:ottplayer:5.0.1.29-sports-test04-SNAPSHOT', store_dir)
    copy_dependencies(
        'com.pptv.ottplayer:xplayer:5.0.1.29-sports-test04-SNAPSHOT', store_dir)
    # copy_dependencies('com.squareup.okhttp3:okhttp:3.10.0', store_dir)


def copy_dependencies(gradleimpl, store_dir):
    # 前提条件：在Anroid Studio中gradle声明依赖，同步gradle以使文件下载到本地。
    groupid0, artifactid0, version0 = gradleimpl.split(':')
    # print(gradleimpl)
    gradle_cache_dir = os.path.join(os.path.expanduser('~'), '.gradle',
                                    'caches', 'modules-2', 'files-2.1')
    groupdir = os.path.join(gradle_cache_dir, groupid0)
    ottplayer_mainlib_path = os.path.join(
        groupdir, artifactid0, version0)
    if not os.path.exists(ottplayer_mainlib_path):
        print("cannot found mainlib %s" % (ottplayer_mainlib_path))
        return
    libs = []
    pompaths = glob.glob("%s/**/**/*.pom" %
                         (ottplayer_mainlib_path), recursive=True)
    if not pompaths:
        print("cannot found %s" % (ottplayer_mainlib_path))
        return
    mainlibmaskname = "%s-%s.*r" % (artifactid0, version0)
    mainlibpaths = glob.glob("%s/**/**/%s" %
                             (ottplayer_mainlib_path, mainlibmaskname), recursive=True)
    if not mainlibpaths:
        print("cannot found %s" % (mainlibmaskname))
        return
    libs.append(mainlibpaths[0])
    if not os.path.exists(pompaths[0]):
        print('cannot found pom path')
        return
    ns = {'pom': 'http://maven.apache.org/POM/4.0.0'}
    tree = ET.parse(pompaths[0])
    root = tree.getroot()
    if not os.path.exists(store_dir):
        os.mkdir(store_dir)
    dependencies = root.find('pom:dependencies', ns)
    if not dependencies:
        return
    for dependency in dependencies:
        scope = dependency.find('pom:scope', ns)
        scopetext = 'compile'
        if scope is not None:
            scopetext = scope.text
        if not scopetext == 'compile':
            continue
        groupid = dependency.find('pom:groupId', ns)
        artifactid = dependency.find('pom:artifactId', ns)
        version = dependency.find('pom:version', ns)
        if version is None:
            print(
                ("WARNING--cannot found version element for %s:%s, "
                 + "unsupport reslove dependency: %s")
                % (groupid.text, artifactid.text, gradleimpl))
            continue
        libdir = os.path.join(
            gradle_cache_dir, groupid.text, artifactid.text, version.text)
        libmaskname = "%s-%s.*r" % (artifactid.text, version.text)
        libpaths = glob.glob("%s/**/**/%s" %
                             (libdir, libmaskname), recursive=True)
        if libpaths:
            # print(libpath)
            libs.append(libpaths[0])

        if 'com.android.' not in groupid.text:
            impl = ':'.join([groupid.text, artifactid.text, version.text])
            copy_dependencies(impl, store_dir)

    for libpath in libs:
        shutil.copy(libpath, store_dir)


def apply_get_migusample():
    """输出聚体育aar集成所用文档"""
    store_path = 'd:\\Work\\TVSportsDemo'
    demoapp_src_path = 'D:\\AndroidStudioProjects\\MiguSample'
    demoapp_dst_path = os.path.join(
        store_path, os.path.basename(demoapp_src_path))
    if os.path.exists(demoapp_dst_path):
        shutil.rmtree(demoapp_dst_path, ignore_errors=True)

    def app_ignore_pattern(path, names):
        return ['build', '3.2bak', '.git', '.gradle', '.idea', '*.idea', 'p.py']
    shutil.copytree(demoapp_src_path, demoapp_dst_path,
                    ignore=app_ignore_pattern)
    print('zipping...')
    shutil.make_archive(store_path, 'zip', store_path)
    print("store path: %s.zip" % (store_path))


def apply_get_javaheap():
    """获取标准的hprof文件"""
    name = '2'
    cache_dir = os.path.join(tutil.get_downloads_path(), 'DumpedHeap')
    tutil.ensure_dir(cache_dir)
    run('adb shell am dumpheap %s /sdcard/1.hprof' % (tconfig.APP_ID))
    # 上面的命令执行完成后还得等待一下才有文件生成
    time.sleep(5)
    run("adb pull /sdcard/1.hprof %s" %
        (os.path.join(cache_dir, "%s.hprof" % (name))))
    run("adb shell rm /sdcard/1.hprof")
    os.chdir(cache_dir)
    oldProf = os.path.join(cache_dir, "%s.hprof" % (name))
    newProf = os.path.join(cache_dir, "droid-%s.hprof" % (name))
    run("hprof-conv %s %s" % (oldProf, newProf))
    tutil.open_file(newProf)


def apply_get_mac():
    """获取Mac地址"""
    print(tutil.get_mac())


def apply_get_gfxinfo():
    """获取gfxinfo"""
    run("adb shell dumpsys gfxinfo %s | grep \"Janky\"" % (tconfig.APP_ID))


def apply_get_meminfo():
    """获取系统及应用的内存信息"""
    pid = tutil.getpid()
    print("--------------------------------")
    run('adb shell "cat /proc/meminfo | grep Mem"')
    print("--------------------------------")
    run("adb shell dumpsys meminfo %s" % (tconfig.APP_ID))
    print("--------------------------------")
    # 可以获取到so库与jar所对应的Pss值，查看内存占用是否有异常。
    run('adb shell "showmap %s | grep %s"' % (pid, tconfig.APP_ID))
def apply_get_heapsize():
    """获取heap大小"""
    result = '---------------------------------------------\n'
    result = result + "设备名称: " + sh("adb shell getprop ro.product.model")
    result = result + "Heap大小：\n" + \
        sh('adb shell "cat /system/build.prop | grep heap"').strip()
    with io.open(os.path.join(tutil.get_downloads_path(),
                              'TV系统heap大小统计.txt'), 'a', encoding='utf-8') as f:
        f.write(result + "\n")


def apply_get_cpucount():
    """获取CPU核数"""
    run("adb shell ls /sys/devices/system/cpu/ | grep -e \"cpu[0-9]\" | wc -l")


def apply_get_gfxinfo():
    """获取GPU绘制信息(gfxinfo)"""
    while 1:
        sh('adb shell dumpsys gfxinfo com.pplive.androidxl | grep "Janky"')


def apply_get_threads():
    """获取应用线程列表"""
    pid = tutil.getpid()
    # 通过进程名找到pid
# ps | grep "system_server"
# # 切换到对应进程的task目录
# cd /proc/507/task
# # 列出各pid对应的文件名
# for t in *; do echo $t `grep Name: $t/status`; done