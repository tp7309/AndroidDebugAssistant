#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import os
import sys
from src.utils import tutil
from src.utils.tutil import sh, run, printcn
import shutil
import tconfig
import io
sys.path.append(os.path.abspath(os.path.join(
    os.path.dirname(__file__), os.pardir, os.pardir, os.pardir)))


NAME = 'clear'


def apply_clear_update():
    """清除升级安装包"""
    sh("adb shell rm /sdcard/Android/data/%s/cache/*.apk" % (tconfig.APP_ID),
       print_msg=False)
    sh("adb shell rm /data/data/%s/cache/*.apk" % (tconfig.APP_ID),
       print_msg=False)


def apply_clear_app():
    """卸载应用"""
    if 'tvsports' in tconfig.APP_ID:
        run('adb uninstall com.pptv.tvsports.launcher')
        run('adb uninstall com.pptv.tvsports.dev')
    elif 'androidxl' in tconfig.APP_ID:
        run('adb uninstall com.pplive.androidxl')
        run('adb uninstall com.pplive.androidxl.dev')
    else:
        run("adb uninstall %s" % (tconfig.APP_ID))


def apply_clear_appcache(appid=tconfig.APP_ID):
    """清除app缓存数据"""
    sh("adb shell pm clear %s" % (appid))
    tutil.restart_app(appid)


def apply_clear_readonly():
    """清除系统只读属性"""
    model = sh("adb shell getprop ro.product.model", print_msg=False)
    printcn("设备型号：%s" % (model))
    tvip = tutil.extract_tvip()
    # Q1盒子
    if 'PPBOX Q1' in model:
        run('adb root')
        run('adb shell "echo 1 > /sys/class/remount/need_remount"')
        run('adb shell "mount -o remount,rw /dev/block/system /system"')
        return

    if 'Android SDK' in model:
        run('adb root')
        if not tutil.checkenv():
            run("adb connect %s" % (tvip))
        run('adb remount')
        return

    # 去除sdcard只读属性
    tvip = tutil.extract_tvip()
    run('adb root')
    if not tutil.checkenv():
        run("adb connect %s" % (tvip))
        if not tutil.checkenv():
            print('no adb devices')
            return
    print("current tvip: %s" % (tvip))
    if not tutil.rooted():
        print("device does not support 'adb root', ignore")
        return
    run('adb shell mount -o rw,remount rootfs /')
    run('adb shell chmod 777 /sdcard')
    # 兼容性处理
    run('adb shell chmod 777 /storage/emulated')

    # 去除/system/app目录只读属性，对sdcard只读属性去除有的机型不可用
    run('adb root')
    if not tutil.checkenv():
        run("adb connect %s" % (tvip))
    run('adb remount')


def apply_clear_tcpdump():
    """清除tcpdump"""
    run('adb shell rm -f /data/local/tcpdump')


def apply_clear_tvfreeline():
    """去除聚体育秒速构建支持文件"""
    run('p clear readonly')
    holder_apk = 'com.pptv.tvsports.dev_debug_1.0.apk'
    run("adb shell rm -r /system/app/%s" % (holder_apk))
    run("adb shell rm -r /system/app/%s/%s" % (tconfig.APP_ID, holder_apk))


def apply_clear_usblock():
    """自动判断当前机型，开启USB安装apk支持"""
    if tutil.isppos():
        apply_clear_ppos_usblock()


def apply_clear_ppos_usblock():
    """开启PPOS USB安装apk支持"""
    holders = [tutil.asserts('ahelpcomfortusb.zip'),
               tutil.asserts('iycoruabcikteerme.zip')]
    usbpath = tutil.usbpath()
    if not usbpath:
        print('can not find usb path')
        return
    print("usbpath: " + usbpath)
    for file in holders:
        shutil.copy(file, usbpath)
