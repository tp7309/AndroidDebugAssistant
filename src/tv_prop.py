#!/usr/bin/env python3
# -*- coding: utf-8 -*-


import os

OUTPUT = 'tv_prop.txt'
GREP = 'findstr' if os.name == 'nt' else 'grep'
KEYWORD = 'pplive'


def get_os_prop():
    print("collecting...")
    temp_path = os.curdir
    os.chdir(os.path.dirname(os.path.abspath(__file__)))

    os.system("echo list packages include %s... > %s" % (KEYWORD, OUTPUT))
    os.system("adb shell pm list packages | %s %s >> %s" %
              (GREP, KEYWORD, OUTPUT))
    os.system(
        "echo -----------------------------------------------------  >> %s" %
        (OUTPUT))
    os.system("echo pm path >> %s" % (OUTPUT))
    os.system("adb shell pm path com.pplive.androidxl >> %s" % (OUTPUT))
    os.system("adb shell pm path com.pplive.androidxl.dev >> %s" % (OUTPUT))

    os.system(
        "echo -----------------------------------------------------  >> %s" %
        (OUTPUT))
    os.system("echo com.pplive.androidxl >> %s" % (OUTPUT))
    os.system("adb shell dumpsys package com.pplive.androidxl | %s %s >> %s" %
              (GREP, 'version', OUTPUT))
    os.system(
        "echo -----------------------------------------------------  >> %s" %
        (OUTPUT))
    os.system("echo com.pplive.androidxl.dev >> %s" % (OUTPUT))
    os.system("adb shell dumpsys package com.pplive.androidxl.dev | %s %s >> %s" %
              (GREP, 'version', OUTPUT))
    os.system(
        "echo -----------------------------------------------------  >> %s" %
        (OUTPUT))
    os.system("echo print sysprop >> %s" % (OUTPUT))
    os.system("adb shell cat /system/build.prop >> %s" % (OUTPUT))

    print("result in %s" % (OUTPUT))
    os.chdir(temp_path)


# get_os_prop()
